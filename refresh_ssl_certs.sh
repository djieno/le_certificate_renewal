#!/bin/bash

# script to move the latest ssl certs for Haproxy
# create on Sunday 5th of May by Gino Eising

function move_certs {

EXPIRATION=$(openssl x509 -checkend $(( 86400 * 30 )) -noout -in /etc/haproxy/certs/wildcard.$i.pem)
if [[ $EXPIRATION == "Certificate will not expire" ]]; then

        echo "Cert $i is going to be replaced"
        cp /home/letsencrypt/wildcard.$i.pem /etc/haproxy/certs/wildcard.$i.pem
else

        echo "Cert $i is valid so not doing anything today"
fi
}

function for_domains {

for i in djieno.com chebna.eu
do
        move_certs
done
}

function reload_haproxy {

DJIENO=wildcard.djieno.com.pem
CHEBNA=wildcard.chebna.eu.pem

if [[ /home/letsencrypt/$DJIENO -ot /etc/haproxy/certs/$DJIENO ]] || [[ /home/letsencrypt/$CHEBNA -ot /etc/haproxy/certs/$CHEBNA ]]; then
        echo "Going reload HAproxy since certificates are newer"
        /etc/init.d/haproxy reload
else
        echo "Certificates aren't newer so not doing anything today"
fi

}

# main
for_domains
reload_haproxy
