# Let's encrypt certificate renewal

[![pipeline status](https://gitlab.com/djieno/le_certificate_renewal/badges/master/pipeline.svg)](https://gitlab.com/djieno/le_certificate_renewal/commits/master)

This CD pipeline will refresh your Let's encrypt certificate(s) for your domain(s) and replace on HAproxy ingress
and also Gitlab Pages certificates. The validity of your current certificate is tested before actual
requesting new certificates.

```mermaid
graph LR
A[Gitlab runner job] -->B{ valid}
style A fill:#EBA

B --> |> 30 days|C[blank]
B -->|< 30 days| E[Certbot]
style E fill:#ABC

E -.-> H
H[Google cloud DNS] -.-> E
style H fill:#ABC,stroke:#333

E --> |LE cert| R[API call]
style R fill:#BDD
R --> X[Gitlab/pages]

E --> |LE cert| S[script]
style S fill:#BDD
S --> |cronjob| Y[HAproxy]
```

The Let's encrypt 2.0 API requires DNS-01 validation of domain ownership. This means that just before the point of
requesting a new certificate from Let's encrypt a txt record in the specific domain needs to be publicly available.
To do this I have pointed to google NS servers with my TransIP domain registrar and use cert-bot/google-cloud-dns
to add/remove the txt records. bladibla is great!

My website runs locally using HAproxy and Kubernetes and secondly on Gitlab Pages which runs on Google App engine.
Whenever my Local internet is not available, the Gitlab pages version will be served only. This is done by adding
two IP records in the A record of my Google Cloud DNS hosted domain. The browser will place all propagated IP's in
a array and cycle through them until it finds one that responds.

In my local hosted version all incoming traffic is handled by HAproxy which runs on my firewall. To prevent having
to install python for cert-bot on my firewall I use cerbot certificate renewal in a Gitlab pipeline using
docker. I have gitlab-runners in Kubernetes which is hosted on premise. Any docker Gitlab-runner DIND docker would
work fine with this CD pipeline.

The renewed certificates will be in the correct HAproxy format and shipped and eventually reloaded in HAproxy. The
Gitlab pages certificates will be replaced by an API call. To prevent having to share elevated firewall rights to
a local runner (in my local network) I've created a separated script activated by a crontjob running on the firewall
This replaces the certificate with a fresh set of certificates and reloads HAproxy configuration.
